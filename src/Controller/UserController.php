<?php

namespace App\Controller;

// Entity
use App\Entity\User;
use App\Entity\MovieList;
// Form
use App\Form\UserEditType;
use App\Form\UserType;
// Service
use App\Service\ImageDataUriProvider;
// Repository
use App\Repository\MovieListRepository;
// Symfony stuff
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    private $passwordEncoder;
    private $imageProvider;

    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        ImageDataUriProvider $imageProvider
    ) {
        $this->passwordEncoder = $passwordEncoder;
        $this->imageProvider = $imageProvider;
    }

    /**
     * Create a new User and it's default favorite list
     * 
     * @Route("/register", name="user.register")
     */
    public function register(
        Request $request
    ) {
        // Init new user
        $user = new User();
        // Init form
        $form = $this->createForm(UserType::class, $user);
        // Handle request
        $form->handleRequest($request);

        // Check the posted form
        if ($form->isSubmitted() && $form->isValid()) {
            // Get Entity manager
            $em = $this->GetDoctrine()->getManager();

            // Hash the password
            $argonEncoder = new \Symfony\Component\Security\Core\Encoder\Argon2iPasswordEncoder();
            
            $hash = $this->passwordEncoder->encodePassword(
                $user,
                $user->getPassword()
            );
            //$hash = $argonEncoder->encodePassword($_POST["user"]["password"], null);
            $user->setPassword($hash);

            $user->setIsBanned(false);

            // Persist the new user
            $em->persist($user);

            // Create default favorite list
            $defaultFavList = new MovieList();
            $defaultFavList->setType("Favorite");
            $defaultFavList->setTitle("Favorite movies");
            $defaultFavList->setImage(
                $this->imageProvider->getImageDataUri("favorite")
            );
            $defaultFavList->setIsPublic(false);
            $defaultFavList->setOwner($user);

            // Persist the new favorite list of the user
            $em->persist($defaultFavList);

            // Flush
            $em->flush();

            // Redirect to login
            return $this->redirectToRoute("app_login");
        }
        // End check posted form

        // Render a register form
        return $this->render('user/register.html.twig', [
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/user/edit", name="user.edit")
     */
    public function edit(
        UserPasswordEncoderInterface $passwordEncoder,
        Security $security,
        Request $request
    ) {





        // Get current user
        $currentUser = $security->getUser();


        // Check if the user is banned
        if ($currentUser->getIsBanned()) {
            return $this->render("situation/banned.html.twig");
        }


        // Create form
        $form = $this->createForm(UserEditType::class, $currentUser);

        // Handle request
        $form->handleRequest($request);

        // Flush if valid
        if ($form->isSubmitted() && $form->isValid()) {


            $em = $this->GetDoctrine()->getManager();

            if ($currentUser->getUsername() !== $_POST["user_edit"]["username"]) {
                // A new name was typed
                $currentUser->setUsername($_POST["user_edit"]["username"]);
            }
            if ($currentUser->getEmail() !== $_POST["user_edit"]["email"]) {
                // A new email was typed
                $currentUser->setEmail($_POST["user_edit"]["email"]);
            }

            if ($_POST["password"] !== "") {
                // A password is typed
                if ($_POST["password"] === $_POST["confirmPassword"]) {
                    // The password and it's confirmation are the same
                    if (!$passwordEncoder->isPasswordValid($currentUser, $_POST["password"])) {
                        // A new password was typed
                        $hash = $passwordEncoder->encodePassword(
                            $currentUser,
                            $_POST["password"]
                        );
                        $currentUser->setPassword($hash);
                    } else {
                    }
                }
            }

            

            $em->persist($currentUser);
            $em->flush();
            return $this->redirectToRoute("user.edit");
        }

        // Render view
        return $this->render("user/edit.html.twig", [
            "form" => $form->createView(),
            "user" => $currentUser
        ]);
    }







    /**
     * @Route("/user/delete/{id}", name="user.delete", requirements={"id"="\d+"}, methods="DELETE")
     */
    public function delete(
        MovieListRepository $movieListRepository,
        Security $security,
        Request $request
    ) {



        // Get the current user
        $currentUser = $security->getUser();





        
        // Check if the user is banned
        if ($currentUser->getIsBanned()) {
            return $this->render("situation/banned.html.twig");
        }





        // Get all movie of the current user
        $movieListOfCurrentUser = $movieListRepository->findBy([
            "owner" => $currentUser
        ]);



        // Check the csrf token
        if ($this->isCsrfTokenValid("delete".$currentUser->getId(), $request->get("_token"))) {
            // The the csrf token is valid, delete the user
            $em = $this->GetDoctrine()->getManager();
            $em->remove($currentUser);
            for ($ii = 0; $ii < sizeof($movieListOfCurrentUser); $ii++) {
                $em->remove($movieListOfCurrentUser[$ii]);
            }
            $em->flush();

            // flash
            $this->addFlash("success", "User successfully deleted");

            //$em->detach($currentUser);

            return $this->redirectToRoute("index");
        } else {
            // The the csrf token is not valid, kill the app
            die("not valid token");
        }



    }
}
