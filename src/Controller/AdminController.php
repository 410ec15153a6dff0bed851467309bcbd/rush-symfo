<?php

namespace App\Controller;

use App\Form\UserEditType;
use App\Repository\UserRepository;
use App\Repository\MovieListRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin.index")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig');


            // ban an user
            // pardon an user
            // Edit an user
            // delete an user

        // Create a promoted list
        // Ban by ajax

        // stat page (most faved movie)







        //
    }

    /**
     * @Route("/admin/user", name="admin.index_user")
     */
    public function indexUser(
        UserRepository $userRepository
    ) {
        $users = $userRepository->findAll();
        return $this->render('admin/user/index.html.twig', [
            "users" => $users
        ]);
    }

    /**
     * @Route("/admin/user/edit/{id}", requirements={"id"="\d+"}, name="admin.edit_user", methods="GET|POST")
     */
    public function editUser(
        UserRepository $userRepository,
        Request $request,
        $id
    ) {
        $user = $userRepository->find($id);
        $form = $this->createForm(UserEditType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $em = $this->GetDoctrine()->getManager();

            // Define default variables
            $postedIsBan = false;
            $postedRole = $_POST["role"];

            // Update username
            if ($user->getUsername() !== $_POST["user_edit"]["username"]) {
                // A new name was typed
                $user->setUsername($_POST["user_edit"]["username"]);
            }

            // Update email
            if ($user->getEmail() !== $_POST["user_edit"]["email"]) {
                // A new email was typed
                $user->setEmail($_POST["user_edit"]["email"]);
            }

            // Update status of ban
            if (!empty($_POST["status"]) && $_POST["status"] === "on") {
                $postedIsBan = true;
            }

            if ($user->getIsBanned() !== $postedIsBan) {
                // A new status was set
                $user->setIsBanned($postedIsBan);
            }

            // Update role
            if ($postedRole === "user") {
                // The role user was posted

                // Or the user is already an user, so nothing,
                // or the user is an admin dismissed to user

                if (!in_array("ROLE_ADMIN", $user->getRoles())) {
                    // The user is already an user
                } else {
                    // The user is an admin, dismiss to user
                    $user->setRoles(["ROLE_USER"]);
                }

            } else if ($postedRole === "admin") {
                // The role admin was posted

                if (in_array("ROLE_ADMIN", $user->getRoles())) {
                    // The user is already an admin
                } else {
                    // The user doesn't have the admin role yet
                    $tmp = $user->getRoles();
                    array_push($tmp, "ROLE_ADMIN");
                    $tmp = array_reverse($tmp);
                    $user->setRoles($tmp);
                }

            } else {
                // Do nothing
            }
            
            


            $em->persist($user);
            $em->flush();




            return $this->redirectToRoute("admin.index_user");


        }



        return $this->render('admin/user/edit.html.twig', [
            "user" => $user,
            "form" => $form->createView()
        ]);
    }



    /**
     * @Route("/admin/user/delete/{id}", requirements={"id"="\d+"}, name="admin.delete_user", methods="DELETE")
     */
    public function deleteUser(
        UserRepository $userRepository,
        MovieListRepository $movieListRepository,
        Request $request,
        $id
    ) {
     
        // Get target user
        $targetUser = $userRepository->find($id);

        // Get all movie of the current user
        $movieListOfTargettUser = $movieListRepository->findBy([
            "owner" => $targetUser
        ]);



        // Check the csrf token
        if ($this->isCsrfTokenValid("delete".$targetUser->getId(), $request->get("_token"))) {
            // The the csrf token is valid, delete the user
            $em = $this->GetDoctrine()->getManager();
            $em->remove($targetUser);
            for ($ii = 0; $ii < sizeof($movieListOfTargettUser); $ii++) {
                $em->remove($movieListOfTargettUser[$ii]);
            }
            $em->flush();

            // flash
            $this->addFlash("success", "User successfully deleted");

            //$em->detach($currentUser);

            return $this->redirectToRoute("admin.index_user");
        } else {
            // The the csrf token is not valid, kill the app
            die("not valid token");
        }

    }


    /**
     * @Route("/admin/list", name="admin.index_list")
     */
    public function indexMovieList(
        MovieListRepository $movieListRepository
    ) {

        $lists = $movieListRepository->findAll();
        
        return $this->render('admin/list/index.html.twig', [
            "lists" => $lists
        ]);

    }

    /**
     * @Route("/admin/statistic", name="admin.index_statistic")
     */
    public function indexStatistic(
        MovieListRepository $movieListRepository,
        UserRepository $userRepository
    ) {




        $users = $userRepository->findAll();
        
        $lists = [];
        for ($ii = 0; $ii < sizeof($users); $ii++) {
            array_push($lists, $users[$ii]->getMovieLists());
        }

        
        return $this->render('admin/statistic/index.html.twig', [
            "lists" => $lists
        ]);

    }
}
