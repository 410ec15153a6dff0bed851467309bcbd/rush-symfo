<?php

namespace App\Controller;

use App\Repository\MovieListRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(
        MovieListRepository $movieListRepository,
        Security $security
    ) {




        // Get current user
        $currentUser = $security->getUser();

        // Check if the user is banned
        if ($currentUser !== null && $currentUser->getIsBanned()) {
            return $this->render("situation/banned.html.twig");
        }






        $publicMovieLists = $movieListRepository->findBy(["isPublic" => true]);
        return $this->render('home/index.html.twig', [
            "movieLists" => $publicMovieLists
        ]);
    }
}
