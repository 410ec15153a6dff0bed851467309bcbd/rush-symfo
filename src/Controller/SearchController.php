<?php

namespace App\Controller;

use App\Repository\MovieListRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchController extends AbstractController
{
    private $defautlImage;
    
    public function __construct()
    {
        $this->defautlImage = "data:image/png;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QNvaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjAtYzA2MCA2MS4xMzQ3NzcsIDIwMTAvMDIvMTItMTc6MzI6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6MDg4MDExNzQwNzIwNjgxMTk3QTVEOEQ1NjM0NkQ4MUIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OEVCNDRFRjIxOTMwMTFFMThCMDQ4QkZDRDEwNDdDNUQiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OEVCNDRFRjExOTMwMTFFMThCMDQ4QkZDRDEwNDdDNUQiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDowODgwMTE3NDA3MjA2ODExOTdBNUQ4RDU2MzQ2RDgxQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDowODgwMTE3NDA3MjA2ODExOTdBNUQ4RDU2MzQ2RDgxQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pv/uAA5BZG9iZQBkwAAAAAH/2wCEAAYEBAQFBAYFBQYJBgUGCQsIBgYICwwKCgsKCgwQDAwMDAwMEAwODxAPDgwTExQUExMcGxsbHB8fHx8fHx8fHx8BBwcHDQwNGBAQGBoVERUaHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fH//AABEIASwBLAMBEQACEQEDEQH/xACLAAEAAgMBAQEAAAAAAAAAAAAAAwQCBQYBBwgBAQAAAAAAAAAAAAAAAAAAAAAQAAEDAgMEBgYECgYKAwAAAAEAAgMRBCESBTFBUQZxgSIyEwdhkaGxUhTB0UIVYnKCsiNzJDR0NpLSQ5NVFqLCU2NUlCU1FzfwM4MRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AP1SgICAgICAgICAgICAgEgYnBBE+6gbteCfRigidqEY7rSenD60EZ1CTc0DpqUGBvp/QOpB587cfF7AgfO3HxD1BBkL+cfCepBmNRd9pgPQaIJG38J2gt9qCZk8L+68E8N6DNAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBBFLdQx4E1PAYoKsl/IcGANHHaUFd8j3mrnE9KDFAQEBAQEBAQEBBIy4mZ3XGnA4hBZj1DdI3rH1ILUcscgqxwPo3oMkBAQEBAQEBAQEBAQEBAQEBAQEBBFNcxRYE1d8I2oKUt3LJhXK3gEECAgICAgICAgICAgICAgIPQSDUGh4hBZhv3twkGYcRtQXY5GSNqw1CDJAQEBAQEBAQEBAQEBAQEBAJDQSTQDaUFG4viatiwHxb0FRAQEBAQEBAQEBAQEBAQEBAQEBBkx72OzNNCgvW9619Gv7LuO4oLKAgICAgICAgICAgICAgwllZE3M49A3lBrp7h8pxwbuagiQEBAQEBAQEGbYpXd1hPUgzFpcH7HrIQe/JXHAetA+SuPh9oQYm1uBtYerH3II3Mc3vAjpFEHiAgICAgICAgtW14WUZJizcd4QXwQQCDUHYUBAQEBAQEBAQEBAQRzTMiZmdt3Dig1ssr5HZnHHcOCDBAQEBAQEE0dpM/GmUcTggsx2EY75Ljw2BBYZFEzutA6kGSAgICAgII320D9rBXiMPcgryafvjd1O+tBVkhlj77aDjuQYICAgICAgntrl0RocWHaOHQg2LXBwDmmoOwoPUBAQEBAQEBAQYSytiYXO6hxKDWSyukeXO27hwQYICAgICCaG1klxAo34igvRW0UeIFXfEUEqAgICAgICAgICAgEAih2IK01ix2LOy7huQUpInxuo8UQYICAgICCxa3Jidld3Dt9CDYggio2FAQEBAQEBAQeOcGtLiaAYkoNZcTmV9fsjuhBEgICAg9AJNAKk7Agu29kBR0uJ3N3daC2gICAgICAgICAgICAgICCpezsDTEAHOO30IKKAgICAgILdlcUIiccD3T9CC8gICAgICAgo30+Z3hNOA73SgqICAgIPWtc5wa0VJ2BBsre2bEKnF52nh0IJkBAQEBAQEBAQEBAQEBAQV7q6EYyM759iDXE1xO1AQEBAQEBAQbK0n8VlD327fT6UE6AgICAgiuZvCiJ+0cG9KDVoCAgIPQCSAMSdgQbK2txE2pxedp4ehBMgICAgICAgICAgICAgICCvdXIiGVuMh9iDXEkmpxJ2lAQEBAQEBAQEGcMpikDx1j0INs0hwBGIOIQEBAQEGsu5fElNO63AIIUBAQEF6yt6DxXDE90ejigtoCAgICAgICAgICAgICAggubkRNoMXnYOHpQa4kuJJNSdpQeICAgIJYIHSvoMGjvFB7dW/hPw7h2FBCgICAgvWE1WmM7Ri3oQW0BAQRXUvhwkjacB1oNWgICAgmtYfFkx7rcXfUg2aAgICAg+fw83a67nc6UZm/JfNOh8PI2uQE0GalUDnrm7XdJ1ptrYzNjhMLH5Sxru0S4HEg8EG55C5iu9a02d168PuoJcrnABtWOALcBTfVBz/OPPOs2GvTWWnTNZBA1jXAsa6ryMzsSDxog6nknVr7VdCZd3rxJOZHtLg0NwacMBRBFz1zFNoulMdauDb24kDISQHUa3F7qHD0daDUch856hqmozWOpSNe9zM9s4NazFneb2QK1Br1IO6QcBzP5iXUd67TtDYHysd4brjLnLn7Msbd+OFSg1s+r+Z1nCb24EwgAq8uhiIAHxNDatHqQdHynzyNYhkguYxHqEQzUZXI9uzMK1pQ7Qg57mfmfXrbX5LOzlGU+GGMLGuJc9o3kcSgi+8PMT/YO/uo/qQbrlm55mmnmGsRlkQaDESxre1XHuoOhQEEkELpX5Rs3ngg2ccbY2BrRQBB5NGJIyw79h4FBqnNLXFpwIwKDxAQEGcUhjka8bjj0INsCCARsOxAQEFC/krIGDY0Y9JQVUBAQEG0tovDiAPeOLkEqAgICAg+TW/8A7NP8c/3lBn5ktDua4WkVBhiBH5bkFnkSYaPzXqOmTOyxkSsqeMDi4H+hmKDm5Wv1Mazq8gwa5sg9D55hQf0Q5B9H8tP5Xj/XSe9BynOVxJr3OUOlwOrHC9tq0jYHE1ld+TsPQgh5ktTyxzhDd2bMlvVlxCwbMvdkZX00PrQfTNSvgNAur+2dUC0kngeN/wCjL2lB8/8AKqyhl1S7upGh0lvG0RE7jISCR6aNog7PX+btH0ecWl5nM0kYka0MzNLXEtx/olBzHL9/yf8AeLIdLgdHdy5g1xa7YAXOFXE0wCDnub5zb82unAzGIwvy7K5WtNEGw/8AJU//AADf7w/1UHQ8s8wP1qCaV0Ah8JwbQOzVqK8Ag3SDOGF8r8res8EG0iiZGwNbs3nigyQEFK/h2Sjod9CCmgICAg2NlJmhodrMOrcgsIBIAqdgQaiR5e9zjvNUGKAgIJrSPPMK7G4lBs0BAQEBAQfJrf8A9mn+Of7ygk8yP5tg/UxfnuQReYttLY80PuYSWfNwh4cPwmmJ46wPagytLDwPLW+unCjru4jLTxZG8NH+lmQdByfqbNM5Bmvn0/QOlLAd7yQGDrcQEHH8uaDzDq8899ps3gzQu7dwZHRuLpAa5XNFdm3pQXeYOUeborF99qd185FaitHTSSuaHEAloePWg6fkC+Zq3K9xpM7u3A10Dt58GYHKertDqQcny3q0vKnMFxBfxnIawXAG4g1a8cR9BQdDrV9yTqDxfahMy4lDAxuR8mbKCSG5GEb3b0HLcpGJ3N0DoGlsBdMY2naGGN+UHbuQWOaJGR86NkecrGPt3OcdgADSSg7L/NPL3/HRes/UgsWWtaXfSmK0uWTSNbnLW1rlBAr7UGwiifI8Nb1ngg2cMLImZW9Z4oM0BAQYyMD2OYd4QakggkHaMCg8QEBBZsH5Zsu5w9oQbBBFdPywPO8inrQatAQEBBsLBlIi/e4+wILKAgICAgINCzkrR26198B03zfimamYZMx9GXZjxQe6zybpGr37b66dKJmNawBjgG0aSRgQeKCXX+VdL110Dr3xA6AODDG4NNHUrWoPBBlPyzpk2hM0R2dtkwNAyuAf2XZq1pvO3BBWdyVpJ0Vuj55xZtl8bB4zF2OBOXZigv6Jodjo1kbSyDvDLzI4vNXFxoKkgDcEFy5t4rm3lt5m5opmOjkbxa4UKDR6RyppHL877q0fMXyMMbmPeHNcKg7A0bKIK+taHpusnPexVlAo2ZhyvA4V3jpQaNvlzo4fU3FwW/DVg9uVBvdM0XTNMYW2cDYye9IcXnpccUFLVOUNJ1K8fd3BlErwAcjgB2RQYUKCp/4+0H4p/wCmP6qDY6Hylp+m3jprMyGV7DGc7gRlJBOwD4UHVQwtiZlG3eeKCRAQEBAQa6+ZlmrucKoK6AgIMo3ZJGu4EFBt0FXUHUY1vE19SCggICAg28TMkTW8AgyQEBAQEBAQEBAQEBBhLKyJmZ3UOKDWSyvkeXO6hwQYICAgIMmMc9wa0VJQbOCBsTKDFx7xQSICAgICAgq6gysbXfCfegoICAgINn4n7Jn35PbSiCvqB/SNHAV9ZQVEBAQZxNzSsbxIQbZAQEBAQEBAQEHJ+Zc00PLrXxPdG75hgzMJaaZXbwg0PlvzTKy8dpN7K57Lg5rZ7ySRJvZU7nDZ6elBb817m4h+6/BlfFm8fNkcW1p4dK0QdBFrMWl8mWeo3JLyyzgIaT2nyOjaAKniTig+eRDmbm67kkfORCw9qpLYWV+y1orU/wDwoF/oHMPLzBeW9yTC0jO+EuGWp+2w4Uqg6TS9Un5j5euYWubDfgeHI4VABOLXimIrRBzmr8ta5pdi+8mv87GFoLWPkr2jTfRBHoeg61rFo+5gvvDYyQxlr3yVqGh1cK/Eg7/QNOurPTYLOeTxrhmbNICTXM8uGLscAUHQ29u2FvFx7xQSoCAgICAgIIrpua3eOAr6sUGrQEBAQW8//T6emntqgwvjWfoAQV0BAQTWgrcM6z7EGzQEBAQEBAQEBByPmh/LTf4mP81yDgZtDnh5dsdetS4DO5k5aTVj2yHw3jhw6elBe5v5gZrekaLcEgXUfjx3TBueBFj0O2oNhzZPK7krRm/YDbdvqgNEG05DYxvLsRaMXSSF/Tmp7gEG11lrHaReteAWmCStfxCg4zy3c/5+7aO4YgXdIdh7yg6Dnn+XLj8aP88IKvlw1ztGma0VJuXUH/5sQd5bW7Ym44vO0oJkBAQEBAQEBB48VY4cQQg06AgICCbN+yU/3n0IPb394d1e5BAgICCex/eB0FBskBAQEBAQEBAQcj5ofy03+Jj/ADXIHJFva3PI7Le6aHW8vjMkad4Lzs9PBB8z1nS5tM1GWzlxyGsb/iYe65B38+knVOTrS2ZQTC2gfCTsztjFB1jBBy/L3MtxoEktjewPdDmq6PY9j6UNAdtUFvmHnhl/ZusrCF8bZuzLJJQOI3ta1pdt6UG65H0ObT7GS4uWllxdUPhna1ja0r6TWqCbnn+XLj8aP88IJPKpjfuG4fTtfNPFfR4caDtEBAQEBAQEBAQEGmQEBAQSf2H5X0IM7394d0D3IIEBAQT2X7w3oPuQbJAQEBAQEBAQEEF5a2VzDkvIY54Qc2SVrXtqNho4HFBrg23jYIbaFlvbsJLIY2hjRXEmjaDFBWudO0+6cHXNtFO5oo10jGvIHAZgUE0cbI2NjjaGRsAaxjRQADAAAIILzTNPvafN20cxGAL2gkdB2oI7TRNItH+JbWkUcg2PDRmHQTigugEmgxJ2BBdGmWssBju4WTtfTNHI0PbhiMHVCCa0srOzjMVpBHbxk5iyJjWNLiAK0aBjggmQEBAQEBAQEBAOAJQaZAQEBBJT9nr+H9CCW/FJ+loQVkBAQS2ppcMPpp68EG0QEGE88NvC+ad7Y4YxmfI40AA3koK1jrOlX73R2V3FcPYMzmxuDiBsrggnurq2tIHXFzI2GBlM8jzRoqaCp6SgisdU06/DzZXMdwI6B5jcHUrsrToQWJZY4o3yyODI42lz3uNAGgVJJQVbHWdJv5HR2V3FcPaMzmxuDiBWlTRBPd3lrZ27ri6lbDAymaR5o0VNBj0oNSzWbLUi75Sdk0cdMwY4OpXZWnQg9c5rWlzjRrRUk7AAgrWmq6bePLLW5jne0ZnNY4OIGyuCD0anppuPlhdwm4rl8HxG583DLWtUFlA2oLD7jT9MjbNf3EdvnOVhkcGitK0Fd6C1Z31new+PaTMnhqW+JGQ4VG0VCDO4uILeF888jYoYxV8jjQAcSUGu/wA18tf4nb/3jUE1rr+iXcohtr+CWU92NsjS49AriguySRxRullcGRsBc97iA1rQKkknYAggtNT029LhZ3cNyWULxDI2SldlcpNEFlBUvtX0uwLBe3UVuZKlgkcG1ptpXpQVf818tf4nb/3jUEtrzBod3O23tr6Gad9ckbHguNASaAegINggxlNInng0+5BqEBAQEE+X9ir+H9FEEuot7THcQR6kFNAQEHrHZXtdwIPqQbhAQcV5o6v8vpMWnRupJePrIP8AdR4+11EHG8uXVzy9zJZyXQMcczWeLXYYbhoIP5NQekIPr+oWUV9Y3FnL/wDXcRujceGYUr1IPmHl7eS6XzTJptx2PHz28jScBLGSW+4jrQdh5i6n8ny3LE00lvHCBtPhPaf/AKIp1oKPlfpYttGm1CQUfePo0n/Zx1A9bsyDWeZ+tmX5bTIj2a+NKBv2tZX2oNLypcT6PzH8ldDw/G/QStO5xxYfXh1oPoV/+43H6p/5pQcJ5cf90uf1H+u1BBD/AD+f4x/vKD6Sgv2lrkpI8dvcOCD5v5g3k+r8zQaRadvwMsLG7jNLQux9AoEFjyt1V0F9daRMcvjVliad0jMHjpLfcg7HnL+V9S/Un3hBwHI/J+ma9a3M15LNG6GRrGiFzAKEVxzNcgw525SsNAbay2dzI8zOIMUpaXjLjmBaG4dSDtbe7urvy8muLpxdPJp8+d52uox4BPSBVBzvlJ+8al+JF73IPpCD5v5t/vGm/iS+9qC3pPlpoV5pVndyz3QkuIIpXhr4w0OewONKxnDFBt9G8v8ARtJ1KHULaa5fNDmytkcwtOdpYagMadjuKDpkEN47Lbu9OCDWICAgILuT/p/p73tQZ37awh3wn3oNegICAg2ts/PAw76UPVggkQfHOadWtdU5udJcPP3fBI2DM3E+FG7tltPiOYhBNz3rehaxLaXGnF3jRNMUrXMyDIMWU6MUH0DkrV/vTl62lc7NPCPAn45mYAnpbQoOG8wLOXSeaYtTt+z4+W4jO4SxEB3uB60HnPer/fmq6daWZzM8KMsbX+1uaOoehuVB30TYbSwgsoTltrWNrA44VDB3j70Hyt+q2l5zR94XpPyglzgAZjkj7jaemgqgy5r1PTr/AFOO+09zg8tAlq0tOZh7Lh1e5B3tnqLdR5d+bHekgd4gG54aQ4esIOS8uP8Aulz+o/12oIIf5/P8Y/3lB9XtLXLSSQdr7I4IMtTvorDT7i9l7lvG55HEgYDrOCD5FyrrOn23MT9W1d7i79JI3K3MTLIcT6iUEeo6xZw82HWNJLjD4zbgNcMpzHGRp9DjX1oPpvNVxFccn3txC7NFNbiSN3FrqEFB845V5Y1jWLeeWwvG2zInhr2lz21JFa9gFBlzDyZrOixMv7p8V3DnAkcHONCTgHhwaaH0IO3s9cZrHId/cNibA+K1uIJImYMa5kJ7voykYINF5SfvGpfiRe9yD6Qg+b+bf7xpv4kvvagr6d5c6td6fbXTNRYxlxEyVrCH9kPaHAYHdVB0fKXJuoaJqUl1cXjbhj4XRBgzYEua6uP4qDrUFPUH4MZ1lBSQEBAQbbw/0Hh/g5fYgTszwvbxGHSg1KAgICC7p8nejP4w+lBBzJe3Fnol3NaxvluchZA2Npc7O/sg0APdrVBw/IXJsN2Lq51mzcWNLY4IpQ9hJ7znU7J4IOmvuQOW5bKeO2s2w3DmOEMoc/svp2Ti470HOeWp1XT9Tnsbq0nit7ltQ58bg1skdTtIpi2vsQdB5i6O/UNBMkLDJc2jxIxrRVxaey9oA6a9SDieSdDunay24uoJIo7Zpe3xGloLz2WjGmytUHWc3XNzDokzLaN8s1x+hAjaXEB3eOH4IIQaLlDlS1msZbjVLUuke/LFHJmaQ1o20BG0n2INjrPJ2kO0y4+StRHdNYXROaXEktxy0JPepRBrOTH38NlqGn3FvKxro3Swl7HAZsuVzRUb8EEfl/ZXkGpXLp4JImmGgL2OaCc44hBSvbfW7PmqfULWwlldFcOkizRSOY7E07tKjrQbr/PfPX+EM/5e4/roMOYdb5j1blmK3m0+RlxPO7xmQwygCKINLah2Y9pzq9SDb8rci6QdDt5dUs/EvJgZH5y9paHHstoCPs0QV+dOR9Oi0c3Gj2eS5he0vZHne57D2SACXbCQUEekSalP5f6jp09tM25tWFkLXRvDnseczctRjQ1GCDQ8vatzZoMM0NnpbpGzOD3GaCYmoFMMpagl1nVOduYo2WUunSMizB3hwwSMBcMAXOfm2V40QdbpnL1xpHI1/ZPGe8nt7iSWNna/SPiLQ1tNuDQMN6DiOXb7mjQXzvs9MfIbgND/ABoJjTLWlMpZxQbv/PfPX+EM/wCXuP66DLzJtdRvRpMsVtJK8wvMojY5wa52Q0NAadaCvZc4c7WdnBaRaSDHbxsiYXW85cWsaGitHDHBBtNE5w5vvNVtra70xsNtK/LLKIJ2loptzOcQOtB3SDV3Umedx3DAdSCJAQEEluzPMxvpqerFBtUBBqp2ZJnN3Vw6CgjQEBBnDJ4crX7ht6EG2BBFRsKAgICChd3WesbD2d54oKqAgICAguWlrWkkgw+y36UF1AQEBAQEBAQEBAQEBAQR3EvhxOdv2DpKDVICAgILensq5z+AoOtBeQEFPUI+7IOg/QgpICAgINhYzZo8h7zdnQgsoCCjd3VaxxnD7TuKCogICAgILdpa5qSSDs/ZHFBeQEBAQEBAQEBAQEBAQEBBr72bPJlHdZ70FZAQEBBtLaPw4WjecT1oJUBBjLGJI3MO8e1BqSCCQdowKDxAQEGcUhjkDxu9yDase17Q5uIOxBUu7rbHGfxnfQgpICAgICC1aWuc53jsbhxQX0BAQEBAQEBAQEBAQEBAQQ3U/hR4d92A+tBrEBAQEEtrF4kwB7oxKDaICAgIKF/DleJBsdt6UFVAQEBBIyeRkbmNNA72II0BAQEBBZtbUyHO/uD2oNgAAKDYgICAgICAgICAgICAgICDGSRsbC5xoAg1csrpXlzuocAgwQEBAQbKzh8OKp7zsT0bkE6AgICDGSMSMLDsKDVPY5jy120IMUBAQEBAQEBBYtbYynM7Bg9qDYgACgwA2BAQEBAQEBAQEBAQEBAQEHjnNa0ucaAbSg1tzcGZ3Bg2BBCgICAgntIPEkqe43E/Ug2SAgICAgIK17b5252jtN2+kINegICAgICAgntrYyuqcGDaeKDZABoAAoBsCAgICAgICAgICAgICAgIMZJGRtzONAg11xcumPBg2BBCgICAg9Yxz3BrcSdiDawxNijDB1niUGaAgICAgICCheW2Q+Iwdg7RwKCqgICAgIPW0qK7N9EG0gfE5gEewbuCCRAQEBAQEBAQEBAQEBAQQz3UcWHef8I+lBr5ZXyuzOPQNwQYICAgICDY2lv4bczu+fYEFhAQEBAQEBAQCAQQRUHaEGtubYxOqMWHYeHoQQICAgICD1rnNNWmh4hBbhv90o/KH1ILbJGPFWOBCDJAQEBAQEBAQEBBHLcRR952PAYlBTmvZH4N7DfagrICAgICAgvWlrlpI8Y/ZbwQW0BAQEBAQEBAQEHjmhwLXCoO0INdc2rojmbizjwQQICAgICAg9BINQaHiEE8d7M3A0cPSgnZqEZ7zS0+jFBK26t3bHgdOHvQSB7DscD0FB7UIBIG1BiZYhteB1hBG68t2/ar0IIX6gPsM6ygryXU79rqDgMEESAgICAgICC9a2mWj5Bj9lvBBbQEBAQEBAQEBAQEBAIBFDiEFG4siKuixG9v1IKiAgICAgICAgICAgICAgICAgICAgIPWMc9wa0VJ3INhb2jY+07F/sCCwgICAgICAgICAgICAgICCCe0ZLiOy/jx6UFCWGSI0eOg7kGCAgICAgICAgICAgICAgICAgIJ4LSSTE9lnE/QgvxQxxNowdJ3lBmgICAgICAgICAgICAgICAgIDmhwo4VB2goKk1g04xmh+E7EFSSKSM0e0j07kGCAgICAgICAgICAgICAglitZpMQKN4lBdhs4o8T2ncTs9SCdAQEBAQEBAQEBAQEBAQEBAQEBAQEAgEUIqEEEllC/YMp9H1IKz7CZvdo4eooIHRyM7zSOkIMUBAQEBAQEBBIy3mf3WHpOHvQTx6e44vdT0BBajtoY+63HicSgkQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBBDJ8p9vJXqr7EFd/3fur1V+lBE75Td4nsQY/s/wCH7ED9n/D9iDNvyW/P10+hBMz7v6/TVBYj8D+zy9VEEiAgICAgICAgICAgICAgIP/Z";
    }

    /**
     * @Route("/search", name="search.index")
     */
    public function index(
        MovieListRepository $movieListRepository,
        Security $security
    ) {



        // Get current user
        $currentUser = $security->getUser();


        // Check if the user is banned
        if ($currentUser !== null && $currentUser->getIsBanned()) {
            return $this->render("situation/banned.html.twig");
        }




        if ($currentUser !== null) {
            // Get the Favorite list
            $favoriteMovieList = $movieListRepository->findBy([
                "owner" => $currentUser,
                "type" => "Favorite"
            ]);
            // Get ride of the index
            $favoriteMovieList = $favoriteMovieList[0];

        }

        $randomMovieName = [
            "Rick and Morty",
            "The Gentlemen",
            "EndGame",
            "La Casa De Papel",
            "The Platform",
            "Contagion",
            "Onward",
            "Bad Boys for Life",
            "The Invisible Man",
            "The Invisible",
            "Underwater",
            "Knives Out",
            "Bloodshot",
            "Gisaengchung",
            "Vivarium",
            "Jumanji: The Next Level",
            "The Call of the Wild",
            "Spenser Confidential",
            "Uncut Gems",
            "Jojo Rabbit",
            "Little Women",
            "Joker",
            "Midsommar",
            "Frozen II",
            "The Way Back",
            "Rocketman",
            "The Shawshank Redemption",
            "Avengers: Endgame",
            "Le Mans",
            "The Lion King",
            "31",
            "Sonic the Hedgehog",
            "Dolittle",
            "Yedinci Kogustaki Mucize",
            "Hogar",
            "Hogar",
            "I Still Believe",
            "I Still Believe",
            "The Irishman",
            "The Irishman",
            "Bombshell",
            "Bombshell",
            "The Godfather",
            "The Godfather",
            "Inception",
            "Inception",
            "Doctor Sleep",
            "Doctor Sleep",
            "Time to Die"
        ];
        $randomMovieYear = [
            "1999",
            "2000",
            "2001",
            "2002",
            "2003",
            "2004",
            "2005",
            "2006",
            "2007",
            "2008",
            "2009",
            "2010"
        ];

        $result = [];
        $searchPost = "";
        $year = "";
        $count = "";
        $pageNumber = 1;
        if (!empty($_POST)) {

            dump($_POST);
            
            $year = $_POST["searchMovieYear"];
            $type = $_POST["searchMovieTypeSelect"];
            //$pageNumber = $_POST["pageNumber"];
            $search = $_POST["searchMovie"];
            $search = str_replace(" ", "+", strtolower($search));
            $apiKey = "aeb310e8";


            
            $url = "http://www.omdbapi.com/?s=".$search."&page=".$pageNumber."&apikey=".$apiKey."&type=".$type."&y=".$year;
            $omdbResult = file_get_contents($url);
            $json = json_decode($omdbResult, true);
            if ($json["Response"] === "True") {
                for ($ii = 0; $ii < sizeof($json["Search"]); $ii++) {
                    
                    
                    


                    
                    // Set default image
                    if ($json["Search"][$ii]["Poster"] === "N/A") {
                        $json["Search"][$ii]["Poster"] = $this->defautlImage;
                    }
                    




                    // Check if the movie is already in save list
                    if (
                        $currentUser !== null
                        &&
                        in_array(
                            $json["Search"][$ii]["imdbID"], $favoriteMovieList->getArrayOfMovieId()
                        )
                    ) {
                        $json["Search"][$ii]["isInFavList"] = true;
                    } else {
                        $json["Search"][$ii]["isInFavList"] = false;
                    }





                }








                $searchPost = $_POST["searchMovie"];
                $result = $json["Search"];
                $count = $json["totalResults"];
            } else {
                // Not respose from API
            }
        } else {
            // Not posted

        }
        return $this->render('search/index.html.twig', [
            "results" => $result,
            "searchPostPlacholder" => $randomMovieName[array_rand($randomMovieName, 1)],
            "searchYearPlacholder" => $randomMovieYear[array_rand($randomMovieYear, 1)],
            "searchPost" => $searchPost,
            "searchYear" => $year,
            "count" => $count,
            "favoriteListId" => $favoriteMovieList->getId()
        ]);
    }

    /**
     * @Route("/search/ajax/{search}", name="search.ajax", methods="GET")
     */
    public function searchAjax($search)
    {
        $response = [];

        if ($search !== "") {

            $isId = false;
            if (substr($search, 0, 2) === "tt" ) {
                // It's an id
                $isId = true;
            }

            
            $searchedMovie = $search;
            $searchedMovie = str_replace(" ", "+", strtolower($searchedMovie));
            $apiKey = "aeb310e8";
            
            
            if ($isId){

                $url = "http://www.omdbapi.com/?i=".$search."&apikey=".$apiKey;
            } else {

                $url = "http://www.omdbapi.com/?s=".$searchedMovie."&apikey=".$apiKey;
            }


            $omdbResult = file_get_contents($url);
            $json = json_decode($omdbResult, true);

            if ($isId) {
                if ($json["Response"] === "True") {
                    // Set default image
                    //if ($json["Poster"] === "N/A") {
                    //    $json["Poster"] = $this->defautlImage;
                    //}
                } else {
                    // Not respose from API
                }
                $response = $json["Title"];
            } else {

                if ($json["Response"] === "True") {
                    // Set default image
                    for ($ii = 0; $ii < sizeof($json["Search"]); $ii++) {
                        if ($json["Search"][$ii]["Poster"] === "N/A") {
                            $json["Search"][$ii]["Poster"] = $this->defautlImage;
                        }
                    }
                    $response = [
                        "results" => $json["Search"],
                    "count" => $json["totalResults"]
                    ];
                } else {
                    // Not respose from API
                }
            }

        }
        return new Response(json_encode($response));
    }






    /**
     * @Route("/search/ajax/multiple/{search}", name="search.ajax.multiple", methods="GET")
     */
    public function searchAjaxMultiple($search)
    {
        $response = [];

        if ($search !== "") {

            

            $arrayOfId = explode("+", $search);
            
            $apiKey = "aeb310e8";
            
            
            for ($ii = 0; $ii < sizeof($arrayOfId); $ii++) {
                $url = "http://www.omdbapi.com/?i=".$arrayOfId[$ii]."&apikey=".$apiKey;
                $omdbResult = file_get_contents($url);
                $json = json_decode($omdbResult, true);
    
                if ($json["Response"] === "True") {
                    // Set default image
                    //if ($json["Poster"] === "N/A") {
                    //    $json["Poster"] = $this->defautlImage;
                    //}
                } else {
                    // Not respose from API
                }
                array_push($response, $json["Title"]);
            }




        }
        return new Response(json_encode($response));
    }





    /**
     * 
     */
    public function searchMovieById($id, $onlyTitle = false) {
        $apiKey = "aeb310e8";
        $url = "http://www.omdbapi.com/?i=".$id."&apikey=".$apiKey;
        $omdbResult = file_get_contents($url);
        $json = json_decode($omdbResult, true);
        if ($onlyTitle) {
            // Only the title is asked
            if ($json["Response"] === "True") {
                return $json["Title"];
            } else {
                // Not respose from API
                return "No response from API";
            }
        } else {
            // The full movie information is asked
            if ($json["Response"] === "True") {
                // Set default image
                if ($json["Poster"] === "N/A") {
                    $json["Poster"] = $this->defautlImage;
                }
                return $json;
            } else {
                // Not respose from API
                return "No response from API";
            }
        }
    }

    /**
     * @Route("/search/show/{imdbId}", name="search.show")
     */
    public function show(
        MovieListRepository $movieListRepository,
        Security $security,
        $imdbId
    ) {
        
        $currentUser = $security->getUser();


        // Check if the user is banned
        if ($currentUser !== null && $currentUser->getIsBanned()) {
            return $this->render("situation/banned.html.twig");
        }




        
        $movieList = null;
        if ($currentUser !== null) {
            $movieList = $movieListRepository->findBy([
                "owner" => $currentUser
            ]);
        }
        // There will always be the favorite movie list

        


        $movieListAlreadyFilled= [];
  

        for ($ii = 0; $ii < sizeof($movieList); $ii++) {

            if (in_array($imdbId, $movieList[$ii]->getArrayOfMovieId())) {
                // The movie is in the list
                $movieListAsArray[$ii]["isInList"] = "isInList";
            } else {
                // The movie isn't in the list
                $movieListAsArray[$ii]["isInList"] = "isNotInList";
            }

            $movieListAsArray[$ii]["id"] = $movieList[$ii]->getId();
            $movieListAsArray[$ii]["title"] = $movieList[$ii]->getTitle();
        }



        $result = [];
        $apiKey = "aeb310e8";
        $url = "http://www.omdbapi.com/?i=".$imdbId."&apikey=".$apiKey."&plot=full";
        $omdbResult = file_get_contents($url);
        $json = json_decode($omdbResult, true);
        if ($json["Response"] === "True") {
            if (!isset($json["Poster"]) || $json["Poster"] === "N/A") {
                $json["Poster"] = $this->defautlImage;
            }
            if (!isset($json["Title"]) || $json["Title"] === "N/A") {
                $json["Title"] = "No title";
            }
            if (!isset($json["Release"]) || $json["Release"] === "N/A") {
                $json["Release"] = "No release";
            }
            if (!isset($json["Runtime"]) || $json["Runtime"] === "N/A") {
                $json["Runtime"] = "No runtime";
            }
            if (!isset($json["Language"]) || $json["Language"] === "N/A") {
                $json["Language"] ="No language";
            }
            if (!isset($json["Production"]) || $json["Production"] === "N/A") {
                $json["Production"] = "No production";
            }
            if (!isset($json["Plot"]) || $json["Plot"] === "N/A") {
                $json["Plot"] = "No plot";
            }
            if (!isset($json["Director"]) || $json["Director"] === "N/A") {
                $json["Director"] = "No director";
            }
            if (!isset($json["Type"]) || $json["Type"] === "N/A") {
                $json["Type"] = "No type";
            }
            if (!isset($json["Actors"]) || $json["Actors"] === "N/A") {
                $json["Actors"] = "No actors";
            }
            if (!isset($json["Ratings"]) || $json["Ratings"] === "N/A") {
                $json["Ratings"] = "No ratings";
            }
            $result = $json;
        }

        return $this->render('search/show.html.twig', [
            "result" => $result,
            "movieList" => $movieListAsArray,
            "movieListAlreadyFilled" => json_encode($movieListAlreadyFilled)
        ]);
    }
}
