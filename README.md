# Rush Symfony

A movie list website, using the OMDb API

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Composer is needed to install the project

```
$ sudo apt install composer
```

### Installing

Get the project and install composer bundles

```
$ composer install
```

Copy the .env to your own .env.dev.local (for dev version), and set your own database informations

```
DATABASE_URL=mysql://{your-db-username}:{your-db-password}@127.0.0.1:3306/{your-db-name}?serverVersion=5.7
```

Create database

```
$ php bin/console doctrine:database:create
```

Migrate doctrine migrations

```
$ php bin/console doctrine:migrations:migrate
```

Start web server
```
$ php bin/console server:run
```

## Built With

* [Bootstrap 4](https://getbootstrap.com/) - Used to quickly manage front view and responsive
* [jQuery](https://jquery.com/) - Included with Bootstrap
* [Bootstrapious](https://bootstrapious.com/) - Used for tutorial for the admin sidebar
* [Chart js](https://www.chartjs.org/) - Used to generate statistic charts
* [Font awesome](https://fontawesome.com/) - Used to include nice icons
* [OMDb API](https://www.omdbapi.com/) - Movie open source API
* [Symfony 4](https://symfony.com/) - Backend Application

## Thingsto do

Things to improve/finish

* Make pagination
* Correct card image
* Create an AJAX Symfony service
* Prevent creating another "Favorite" movie list
* Correct bug on delete user, session killed, need to init anon session
* Prevent access to feature with roles
* Improve the "paletteMaker.js" to make correct graduated palette
* Add Webpack Encore
* Make an API call total count in admin page
* Convert native Ecmas to jQuery
* Sleep

## Authors

* **Nico Bogucki**