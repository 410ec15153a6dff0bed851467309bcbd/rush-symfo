window.addEventListener("load", function() {

    // Clear search bar and result when button clicked
    document.querySelector("#searchClear").addEventListener("click", function() {
        // Clear search bar
        document.querySelector("#searchMovie").value = "";
        // Clear search result
        document.querySelector("#rowResultList").innerHTML = "";
        // Clear search count
        document.querySelector("#rowResultCount").firstElementChild.innerHTML = "";
        // Clear search pagination
        document.querySelector("#rowResultPanigation").firstElementChild.innerHTML = "";
    });





    let searchMovieType = document.querySelector("#searchMovieType");
    let searchMovieTypeSelect = document.querySelector("#searchMovieTypeSelect");
    let searchMovieInput = document.querySelector("#searchMovie");







    let favButtons = document.querySelectorAll(".quickFav");
    favButtons.forEach(function(ii) {
        ii.addEventListener("click", function() {




            
            listId = document.querySelector("#favoritListId").value;
            movieId = this.id;



            // Init ajax object
            if (window.XMLHttpRequest) {
                // Mozilla, Safari, IE7+ ...
                httpRequest = new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                // IE 6 and older
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }

            // Define method to call by the button class




            // Create loadding spinner
            let spinner = document.createElement("div");
            spinner.setAttribute("class", "spinner-border spinner-border-sm text-light")
            spinner.setAttribute("role", "status")
            let spin = document.createElement("span");
            spin.setAttribute("class", "sr-only");
            spinner.append(spin);


            if (this.classList.contains("addToFavList")) {
            // If add

                httpRequest.open(
                    "GET",
                    "/list/add/"+listId+"/"+movieId,
                    true
                );
                httpRequest.send()
                httpRequest.onreadystatechange = function() {
                    if (
                        httpRequest.readyState === 2 ||
                        httpRequest.readyState === 3
                    ) {
                        // Make loading spin
                        ii.innerHTML = "";
                        ii.append(spinner);
                    } else if (httpRequest.readyState === 4) {


                        if (httpRequest.status === 200) {
                            let serverResponse = httpRequest.response;
                            console.log(serverResponse);
    
                            ii.classList.remove('btn-success');
                            ii.classList.remove('addToFavList');
                            ii.classList.add('btn-danger');
                            ii.classList.add('removeFromFavList');
                            ii.innerHTML = "Remove from Favorite"
    
                        } else if (httpRequest.status === 404) {
                            // Error   
                        } else if (httpRequest.status === 500) {
                            console.log("server error");
                        }



                    }
                }


            } else if (this.classList.contains("removeFromFavList")) {

            // If remove

                httpRequest.open(
                    "GET",
                    "/list/remove/"+listId+"/"+movieId,
                    true
                );
                httpRequest.send()
                httpRequest.onreadystatechange = function() {
                    if (
                        httpRequest.readyState === 2 ||
                        httpRequest.readyState === 3
                    ) {
                        // Make loading spin

                        ii.innerHTML = "";
                        ii.append(spinner);

                    } else if (httpRequest.readyState === 4) {
                        if (httpRequest.status === 200) {
                            let serverResponse = httpRequest.response;
                            console.log(serverResponse);
    
                            ii.classList.remove('btn-danger');
                            ii.classList.remove('removeFromFavList');
                            ii.classList.add('btn-success');
                            ii.classList.add('addToFavList');
                            ii.innerHTML = "Add to Favorite";
    
                        } else if (httpRequest.status === 404) {
                            // Error   
                        } else if (httpRequest.status === 500) {
                            console.log("server error");
                        }
                    }
                }

            } else {



                console.log("no class")
            }




            









        });
    
    });
    // End foreach
                
});