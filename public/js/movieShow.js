window.addEventListener("load", function() {
    
    
    
    
    
    
    
    
    let addListButton = document.querySelector(".addListButton");
    
    // Edit button to "add" of "remove" at the begining
    changeButton(addListButton);


    // Edit button to "add" of "remove" for each select changes
    let addListSelect = document.querySelector("select[name=quickAddList]");
    addListSelect.addEventListener("change", function() {
        changeButton(addListButton)
    });
    




    
    addListButton.addEventListener("click", function() {



            listId = document.querySelector("select[name=quickAddList]").value;
            movieId = document.querySelector("#movieId").value;


            // Init ajax object
            if (window.XMLHttpRequest) {
                // Mozilla, Safari, IE7+ ...
                httpRequest = new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                // IE 6 and older
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            }

            // Define method to call by the button class




            // Create loadding spinner
            let spinner = document.createElement("div");
            spinner.setAttribute("class", "spinner-border spinner-border-sm text-light")
            spinner.setAttribute("role", "status")
            let spin = document.createElement("span");
            spin.setAttribute("class", "sr-only");
            spinner.append(spin);



            if (this.classList.contains("addToList")) {
            // If add

                httpRequest.open(
                    "GET",
                    "/list/add/"+listId+"/"+movieId,
                    true
                );
                httpRequest.send()
                httpRequest.onreadystatechange = function() {
                    if (
                        httpRequest.readyState === 2 ||
                        httpRequest.readyState === 3
                    ) {
                        // Make loading spin
                        addListButton.innerHTML = "";
                        addListButton.append(spinner);
                    } else if (httpRequest.readyState === 4) {


                        if (httpRequest.status === 200) {
                            let serverResponse = httpRequest.response;
    
                            addListButton.classList.remove('btn-success');
                            addListButton.classList.remove('addToList');
                            addListButton.classList.add('btn-danger');
                            addListButton.classList.add('removeFromList');
                            addListButton.innerHTML = "Remove"

                            document.
                            querySelector("select[name=quickAddList]").
                            selectedOptions[0].
                            classList.
                            remove('isNotInList')

                            document.
                            querySelector("select[name=quickAddList]").
                            selectedOptions[0].
                            classList.
                            add('isInList')


                            changeButton(addListButton);

    
                        } else if (httpRequest.status === 404) {
                            // Error   
                        } else if (httpRequest.status === 500) {
                            console.log("server error");
                        }



                    }
                }


            } else if (this.classList.contains("removeFromList")) {

            // If remove

                httpRequest.open(
                    "GET",
                    "/list/remove/"+listId+"/"+movieId,
                    true
                );
                httpRequest.send()
                httpRequest.onreadystatechange = function() {
                    if (
                        httpRequest.readyState === 2 ||
                        httpRequest.readyState === 3
                    ) {
                        // Make loading spin

                        addListButton.innerHTML = "";
                        addListButton.append(spinner);

                    } else if (httpRequest.readyState === 4) {
                        if (httpRequest.status === 200) {
                            let serverResponse = httpRequest.response;
    
                            addListButton.classList.remove('btn-danger');
                            addListButton.classList.remove('removeFromList');
                            addListButton.classList.add('btn-success');
                            addListButton.classList.add('addToList');
                            addListButton.innerHTML = "Add";

                            document.
                            querySelector("select[name=quickAddList]").
                            selectedOptions[0].
                            classList.
                            remove('isInList')

                            document.
                            querySelector("select[name=quickAddList]").
                            selectedOptions[0].
                            classList.
                            add('isNotInList')

                            changeButton(addListButton);

    
                        } else if (httpRequest.status === 404) {
                            // Error   
                            console.log("server error 404");
                        } else if (httpRequest.status === 500) {
                            console.log("server error 500");
                        }
                    }
                }

            } else {
                console.log("no class")
            }



        });


        function changeButton(button) {
            foo = button.parentElement.parentElement.firstElementChild.selectedOptions[0];
            if (foo.classList.contains("isNotInList")) {
                button.innerHTML = "Add";
                button.className = "btn btn-success addListButton addToList";
            } else if (foo.classList.contains("isInList")) {
                button.innerHTML = "Remove";
                button.className = "btn addListButton btn-danger removeFromList";
            }
        }

    


});