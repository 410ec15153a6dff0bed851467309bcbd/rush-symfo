window.addEventListener("load", function() {


    
    //let arrayInput = document.querySelector("#movie_list_arrayOfMovieId").parentElement;
    //arrayInput.setAttribute("style", "display: none;")

    
    // Add encryption to save file
    document.querySelector("form").setAttribute("enctype", "multipart/form-data");
  

    // Clear search bar and result when button clicked
    document.querySelector("#searchClear").addEventListener("click", function() {
        // Clear search bar
        document.querySelector("#searchMovie").value = "";
        // Clear search result
        document.querySelector("#rowResultList").firstElementChild.innerHTML = "";
        // Clear search count
        document.querySelector("#rowResultCount").firstElementChild.innerHTML = "";
    })


    // Trigged enter button to launch search rather than save the movie list
    searchMovieInput = document.querySelector("#searchMovie");
    searchMovieInput.addEventListener("focusin", function() {
        searchMovieInput.addEventListener("keydown", function(event) {
            if (event.key === "Enter") {
                // Prevent submitting form
                event.preventDefault();
                // Launch search instead
                searchButton.click();
            }
        });


    });
    //searchMovieInput.addEventListener("focusout", function() {
    //    console.log("lose")
    //});


    
    let listOfMovieId = [];
    let listOfMovieName = [];

    // If the hidden list of movie id is already filled, ge values
    foo = document.querySelector("#hiddenMovieList").value;
    if (foo === "null") {
        // Empty, do nothing
    } else {
        // Filled
        listOfMovieId = JSON.parse(foo);
        // Get list of name
        hiddenInputName = document.querySelector("#hiddenMovieListName");
        listOfMovieName = JSON.parse(hiddenInputName.value);
        hiddenInputName.remove();
        createMovieList(listOfMovieId, listOfMovieName)
    }
    



    // Create a list of added movie
    function createMovieList(listOfMovieId, listOfMovieName) {
        let filmList = document.querySelector("#rowFilmList").firstElementChild;

        if (listOfMovieId.length == 0) {
            // The array is empty
            filmList.innerHTML = "x";
        } else {
            // The array isn't empty

            // Clear the list
            filmList.innerHTML = ""

            listTitle = document.createElement("p");
            //listTitle.setAttribute("class", "mt-4");
            listTitle.innerHTML = "Current movie(s) in the list:"
            filmList.append(listTitle);

            let ul = document.createElement("ul");
            ul.setAttribute("class", "list-group");
            
            for (ii = 0; ii<listOfMovieId.length; ii++) {
                li = document.createElement("li");
                li.setAttribute("class", "list-group-item");



                deleteButton = document.createElement("button");
                deleteButton.setAttribute("class", "btn btn-danger");
                deleteButton.setAttribute("id", "deleteMovie");
                deleteButton.setAttribute("type", "button");
                deleteButton.setAttribute("style", "float: right;");
                deleteButton.innerHTML = "Delete";

                li.innerHTML = listOfMovieName[ii];


                span = document.createElement("span");
                span.setAttribute("style", "display: none;");
                span.innerHTML = listOfMovieId[ii];
                li.append(span);
                li.append(deleteButton);
                ul.append(li);
            }
            filmList.append(ul);
        }


        hiddenInput = document.querySelector("#hiddenMovieList");
        hiddenInput.value = ""
        hiddenInput.value = JSON.stringify(listOfMovieId);

        // Make delete button working
        setDeleteMovieListener();
    }




    function setDeleteMovieListener() {
        deleteMovieButton = document.querySelectorAll("#deleteMovie");
        deleteMovieButton.forEach(function(deleteButton) {
            deleteButton.addEventListener("click", function(){                
                // Remove the li
                this.parentElement.remove()
                // Remove the movie title
                name = this.parentElement.childNodes[0].data;
                nameIndex = listOfMovieName.indexOf(name)
                if (nameIndex > -1) {
                    listOfMovieName.splice(nameIndex, 1);
                }
                // Remove the movie id
                id = this.parentElement.children[0].innerHTML;
                idIndex = listOfMovieId.indexOf(id)
                if (idIndex > -1) {
                    listOfMovieId.splice(idIndex, 1);
                }
                // Update hidden input
                hiddenInput = document.querySelector("#hiddenMovieList");
                hiddenInput.value = ""
                hiddenInput.value = JSON.stringify(listOfMovieId);
            });
        });
    }




    let searchButton = document.querySelector("#searchButton");
    searchButton.addEventListener("click", function() {

        // Get the value of the search input
        let searchValue = document.querySelector("#searchMovie").value;

        // Init ajax object
        if (window.XMLHttpRequest) {
            // Mozilla, Safari, IE7+ ...
            httpRequest = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            // IE 6 and older
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        }

        // Get info from server
        httpRequest.open("GET", "/search/ajax/"+searchValue, true);
        httpRequest.send();
        httpRequest.onreadystatechange = function() {
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                    let serverResponse = httpRequest.response;
                    createList(serverResponse);
                } else if (httpRequest.status === 404) {
                    // Error   
                }
            }
        }

        // Create list from server info
        function createList(serverResponse) {
            let resultOject = JSON.parse(serverResponse);

            let result = $.map(resultOject, function(value, index) {
                return [value];
            });

            // Show count row
            let rowResultCount = document.querySelector("#rowResultCount");
            rowResultCount.removeAttribute("style");
            let rowResultCountMessage;
            let isResult = true
            if (Array.isArray(result) && result.length == 0) {
                rowResultCountMessage = "No result for "+searchValue;
                isResult = false;
            } else if (Number(result[1]) === 1) {
                rowResultCountMessage = "1 result for "+searchValue;
            } else if (Number(result[1]) > 1) {
                rowResultCountMessage = result[1]+" results for "+searchValue;
            } else {
                // Do nothing
            }
            rowResultCount.firstElementChild.innerHTML = "";
            rowResultCount.firstElementChild.innerHTML = "<p>"+rowResultCountMessage+"</p>";
            // End show count row

            if (isResult) {

                // Show list row
                let rowResultList = document.querySelector("#rowResultList");
                rowResultList.removeAttribute("style");
                rowResultList.firstElementChild.innerHTML = "";
                // End show list row
            
                for (ii = 0; ii < result[0].length; ii++) {
                    // Create main card div
                    let divCard = document.createElement("div");
                    divCard.setAttribute("class", "card flex-row flex-wrap mb-4")

                    // IMAGE

                    // Create img card div
                    let divCardImg = document.createElement("div");
                    divCardImg.setAttribute("class", "card-header p-0")
                    // Create image
                    let cardImg = document.createElement("img");
                    cardImg.setAttribute("class", "card-img cardImgThumb")
                    cardImg.setAttribute("src", result[0][ii].Poster)
                    cardImg.setAttribute("alt", "Movie poster of "+result[0][ii].Title)
                    cardImg.setAttribute("title", result[0][ii].Title)
                    // Append image to card img
                    divCardImg.append(cardImg);
                    // Append card img to main card
                    divCard.append(divCardImg);

                    // CONTENT

                    // Create body card div
                    let divCardBody = document.createElement("div");
                    divCardBody.setAttribute("class", "card-body py-0")
                    // Create title row
                    let titleRow = document.createElement("div");
                    titleRow.setAttribute("class", "row");
                    // Create title col
                    let titleCol = document.createElement("div");
                    titleCol.setAttribute("class", "col-12");
                    // Create title content
                    let title = document.createElement("h4");
                    title.setAttribute("class", "card-title text-center");
                    title.innerHTML = result[0][ii].Title
                    // Append the title to the col
                    titleCol.append(title);
                    // Append the col to the row
                    titleRow.append(titleCol);
                    // Append the row to the body card
                    divCardBody.append(titleRow);
                    // Append body to the main card
                    divCard.append(divCardBody);

                    // Create button row
                    let buttonRow = document.createElement("div");
                    buttonRow.setAttribute("class", "row");
                    // Create button add col
                    let buttonAddCol = document.createElement("div");
                    buttonAddCol.setAttribute("class", "col-6");
                    // Create button add
                    let buttonAdd = document.createElement("button");
                    buttonAdd.setAttribute("class", "addButton btn btn-block btn-primary");
                    buttonAdd.setAttribute("id", result[0][ii].imdbID);
                    buttonAdd.setAttribute("type", "button");
                    buttonAdd.innerHTML = "Add to list";
                    // Append the button add to the col
                    buttonAddCol.append(buttonAdd);
                    // Append the col to the row
                    buttonRow.append(buttonAddCol);
                    // Append the row to the body card
                    divCardBody.append(buttonRow);

                    // Create button add col
                    let buttonSeeCol = document.createElement("div");
                    buttonSeeCol.setAttribute("class", "col-6");
                    // Create button add
                    let buttonSee = document.createElement("a");
                    buttonSee.setAttribute("class", "btn btn-block btn-secondary");
                    buttonSee.setAttribute("href", "/search/show/"+result[0][ii].imdbID);
                    buttonSee.innerHTML = "See Movie page";
                    // Append the button add to the col
                    buttonSeeCol.append(buttonSee);
                    // Append the col to the row
                    buttonRow.append(buttonSeeCol);
                    // Append the row to the body card
                    divCardBody.append(buttonRow);

                    // Append body to the main card
                    divCard.append(divCardBody);
                    
                    // Append the finished card to the row list
                    rowResultList.firstElementChild.append(divCard);
                }

            } else {
                // No result 
            }
            // Set listeners
            setAddListener();
        }








        function setAddListener() {
            let movies = document.querySelectorAll(".addButton");
            movies.forEach(function(ii) {
                ii.addEventListener("click", function(){
                    

                    if (listOfMovieId.includes(this.id)) {
                        // The id is already in movie list
                    } else {
                        // The id isn't yet in movie list
                        listOfMovieId.push(this.id);
                        listOfMovieName.push(
                            this.
                            parentElement.
                            parentElement.
                            parentElement.
                            firstElementChild.
                            firstElementChild.
                            firstElementChild.
                            innerHTML
                        );
                        // Update list
                        createMovieList(listOfMovieId, listOfMovieName);
                    }

                    //console.log(listOfMovieId)
                });
            });
        }



    });
});