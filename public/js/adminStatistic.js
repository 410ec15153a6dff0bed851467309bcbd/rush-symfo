
window.addEventListener("load", function() {


    let getter = document.querySelector("#valueFromTwig");
    values = JSON.parse(getter.value);
    getter.remove();
    
    
    // Create loading spinner
    spinnerContainer = document.createElement("div");
    spinnerContainer.setAttribute("class", "d-flex justify-content-center chartLoading");
    spinner = document.createElement("div");
    spinner.setAttribute("class", "spinner-border");
    spinner.setAttribute("role", "status");
    spinnerContainer.append(spinner);

    document.querySelector(".chart").append(spinnerContainer);






    
    lists = $.map(values, function(value, index) {
        return [value];
    });
    lists[0].pop();







    var listOfIdAndOccurence = new Object();
    var occurenceCount = 0;
    for (ii = 0; ii < lists[0].length; ii++) {
        listOfId = lists[0][ii].listOfId;
        listOfId.pop();
        for (yy = 0; yy < listOfId.length; yy++) {            
            if (listOfIdAndOccurence[listOfId[yy]] !== undefined ) {
                // The id is in array, increment occurence
                listOfIdAndOccurence[listOfId[yy]]++;
            } else {
                // The id is not in array, set key and occurence to 1
                listOfIdAndOccurence[listOfId[yy]] = 1;
                occurenceCount++;
            }
        }
    }
    
    







    let movieCount = 0;
    let arrayOfMovieCount = [];
    let arrayOfMovieId = [];
    for (ii = 0; ii < occurenceCount; ii++) {
        movieCount += listOfIdAndOccurence[Object.keys(listOfIdAndOccurence)[ii]];
        arrayOfMovieCount.push(listOfIdAndOccurence[Object.keys(listOfIdAndOccurence)[ii]]);
        arrayOfMovieId.push(Object.keys(listOfIdAndOccurence)[ii]);
    }










    
    
    
    
    
    // Init ajax object
    if (window.XMLHttpRequest) {
        // Mozilla, Safari, IE7+ ...
        httpRequest = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        // IE 6 and older
        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
    }


    


    let arrayOfMovieTitle = [];
    // Get info from server
    httpRequest.open("GET", "/search/ajax/multiple/"+arrayOfMovieId.join("+"), true);
    httpRequest.send();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                let serverResponse = httpRequest.response;
                arrayOfMovieTitle = (JSON.parse(serverResponse))
                createChart(arrayOfMovieTitle, arrayOfMovieCount)
                document.querySelector(".chartLoading").remove();
                // Destroy load
            } else if (httpRequest.status === 404) {
                // Error   
            }
        }
    }
        




    // Total number of movie
    //movieCount
    // Get value of an index
    //listOfIdAndOccurence[Object.keys(listOfIdAndOccurence)[1]]
    // Get the key of an index
    //Object.keys(listOfIdAndOccurence)[1]







    function createChart(titles, count){

        //doughnut
        var ctxD = document.getElementById("doughnutChart").getContext('2d');
        var myLineChart = new Chart(ctxD, {
            plugins: [ChartDataLabels],
            type: 'pie',
            data: {
                //labels: ["Tamer", "Green", "Yellow", "Grey", "Dark Grey"],
                labels: titles,
                datasets: [{
                    data: count,
                    //data: [300, 50, 100, 40, 120],
                    backgroundColor: getPalette(count.length),
                    //backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
                    hoverBackgroundColor: []
                    //hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
                }]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'right',
                    labels: {
                        padding: 20,
                        boxWidth: 10
                    }
                }


                ,










                plugins: {
                    datalabels: {
                        formatter: (value, ctx) => {
                            let sum = 0;
                            let dataArr = ctx.chart.data.datasets[0].data;
                            dataArr.map(data => {
                                sum += data;
                            });
                            let percentage = (value * 100 / sum).toFixed(2) + "%";
                            return percentage;
                        },
                        color: 'white',
                        labels: {
                            title: {
                                font: {
                                    size: '16'
                                }
                            }
                        }
                    }
                }


















            }
        });
    }



});